To create Jupyter kernel for this exercise, run:

`poetry run python -m ipykernel install --user --name <KERNEL_NAME>`

where <KERNEL_NAME> is a name of your choosing.